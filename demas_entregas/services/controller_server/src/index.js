const Koa = require("koa");
const cors = require("@koa/cors");
const koaBody = require("koa-body");
const Router = require("@koa/router");
const fetch = require('node-fetch');
const fs = require('fs');
const app = new Koa();
const router = new Router();
const port = process.env.PORT || 3001;

const hostStats = "http://stats_server:4000";
const configGet = {
  method: 'get',
  headers: {
    "Content-Type": "application/json"
  }
}
router.post("/game", async (ctx) => {
  // 1. post mutation to Stats server to store new game
  // 2. return state
  const date = new Date();
  const current_date = date.toUTCString()
  const mutationGame =
    `mutation{
      CreateGame(id: "${ctx.request.body.user}-${current_date}", name: "${ctx.request.body.user}")
    }`
    console.log('mutatiom=> ',mutationGame);
  const response = await fetch(`${hostStats}`, {
    method: 'post',
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ query: mutationGame }),
  })
  const res = await response.json();
  console.log('response ', res);
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    idGame: `${res.data.CreateGame}`
  });
});

router.get("/game/:id", async (ctx) => {
  // 1. get current state from Stats server
  // 2. return state
  const queryGame =
    `query{
      Game(id: "${ctx.params.id}"){
        attempts
        word
        guessed_letters
        wordToGuess
      }
    }`
  const response = await fetch(`${hostStats}`, {
    method: 'post',
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ query: queryGame }),
  })
  const stateGame = await response.json();
  console.log('response ', stateGame);
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    ...stateGame
  });
});
const writeFileGame = (name, content) => {
  fs.appendFile(`${__dirname}/store/${name}.txt`, content, (err) => {
    if (err) throw err;
    console.log('Saved!');
  });
}
const readFileGame = (name) => {
  try {
    const data = fs.readFileSync(`${__dirname}/store/${name}.txt`, 'utf8')
    const splitData = data.split(`\n`)
    return JSON.parse(splitData[splitData.length - 1])
  } catch (err) {
    console.error(err)
  }
}
router.get('/files', (ctx) => {
  const files = readDirGame();
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    files
  });
})
const readDirGame = () => {
  try {
    const files = fs.readdirSync(`${__dirname}/store/`)
    return files
  } catch (err) {
    console.error(err)
  }
}
router.post("/game/:id/event", async (ctx) => {
  // 1. get current state from Stats server
  const queryGame =
    `query{
      Game(id: "${ctx.params.id}"){
        attempts
        word
        guessed_letters
        wordToGuess
      }
    }`
  const responseState = await fetch(`${hostStats}`, {
    method: 'post',
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ query: queryGame }),
  })
  const state = await responseState.json();
  console.log('responseStateInitial =>',state);
  // 2. get next state from Game server
  // 3. post mutation to Stats server to store next state
  // 4. return state
  let response = '', config = {};
  switch (ctx.request.body.event) {
    case 'setWord':
      config = {
        method: 'post',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ word: ctx.request.body.word })
      }
      const responseSetWord = await fetch('http://game_server:3000/set-word', config);
      responseGameServer = await responseSetWord.json();
      console.log('responseGameServer ', responseGameServer);
      if(responseGameServer.valid){
      const mutationGame =
        `setWord{
        setWord(word: "${ctx.request.body.word}")
       }`
      const response1 = await fetch(`${hostStats}`, {
        method: 'post',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ query: mutationGame }),
      })
      const res = await response1.json();
      const guessed_letters=responseGameServer.guessed_letters
      const wordTo=responseGameServer.wordToGuess
      console.log('guessed => ', guessed_letters);
      console.log('wordTo => ', wordTo);
      const mutations2 =`mutation{
        Game(game:{
          id: "${ctx.params.id}", 
          player:
            {name: "${ctx.request.body.user}"}, 
          attempts: ${responseGameServer.attempts}, 
          word: "${responseGameServer.word}",
          guessed_letters: ${JSON.stringify(guessed_letters || [])}, 
          wordToGuess: ${JSON.stringify(wordTo || [])}
        }){
          id
        }
      }`
          
      console.log('mutation => ',mutations2);
      const response2 = await fetch(`${hostStats}`, {
        method: 'post',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ query: mutations2}),
      })
      console.log("reponseSetWord stats Game", await response2.json());
      }
    response=responseGameServer
      break;
    case 'guessLetter':
      config = {
        method: 'post',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ ...state.data.Game, letter: ctx.request.body.letter })
      }
      const responseGuessLetter = await fetch('http://game_server:3000/guess-letter', config);
      response = await responseGuessLetter.json();
      console.log('responseess', response);
      const mutations2 =`mutation{
        Game(game:{
          id: "${ctx.params.id}", 
          player:
            {name: "${ctx.request.body.user}"}, 
          attempts: ${response.attempts}, 
          word: "${response.word}",
          guessed_letters: ${JSON.stringify(response.guessed_letters || [])}, 
          wordToGuess: ${JSON.stringify(response.wordToGuess || [])}
        }){
          id
        }
      }`
          
      console.log('mutation => ',mutations2);
      const response2 = await fetch(`${hostStats}`, {
        method: 'post',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ query: mutations2}),
      })
      console.log("reponseSetWord stats Game", await response2.json());
      break;
    default:

      break;
  }
  console.log('id', ctx.params.id);
  ctx.response.set("Content-Type", "application/json");
  console.log(response);
  ctx.body = JSON.stringify({
    ...response
  });
});

app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(port);
