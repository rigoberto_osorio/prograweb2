const { response, request } = require('express');
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
app.use(express.json());

const isValidWord = (word) => {
    const split = word.split(' ');
    if (split.length > 1)
        return false;
    else
        return split[0] === '' ? false : true

}


app.get('/', (req, res) => {
    res.send('Welcome to my server')
})

app.post('/set-word', (req, res) => {
    console.log('request', req.body);
    let response = {}
    if (isValidWord(req.body.word)) {
        response = {
            valid: true,
            attempts: 6,
            word: req.body.word,
            guessed_letters: [],
            wordToGuess: req.body.word.split('').map(ele => '-'),
            completed: false
        }
        res.send(response)
    }
    else {
        res.status(400).send({ error: 'Invalid word' });
    }
})

const isNewLetter = (guessed, letter) => {
    const isNew = guessed.find(le => le === letter)
    return typeof (isNew) === 'undefined' ? true : false
}

const updateArray = (word, arrayWords, letter) => {
    const response = {array:[], found:false};
    [...word].forEach((ele, index) => {
        if (ele === letter) {
            response.found = true
            arrayWords[index] = letter;
        }
    })
    response.array = [...arrayWords];
    return response;
}

const isCompleted = (array) => {
    const completed = array.find(le => le === '-')
    return typeof (completed) === 'undefined' ? true : false
}

app.post('/guess-letter', (req, res) => {
    const request = req.body;
    if ((request.letter === "undefined" || request.guessed_letters === "undefined" || request.word === "undefined" || request.attempts === "undefined" || request.wordToGuess === "undefined"))
        throw new Error("find of undefined")
    if (!isNewLetter(request.guessed_letters, request.letter))
        res.status(400).send({ error: 'Repeated letter' });

    request.guessed_letters.push(request.letter)
    const updated = updateArray(request.word, request.wordToGuess, request.letter)
    console.log('updated', updated);
    if (isCompleted(updated.array)) {
        const response = {
            attempts: updated.found ? request.attempts : request.attempts - 1,
            word: request.word,
            guessed_letters: request.guessed_letters,
            wordToGuess: updated.array,
            completed: true
        }
        res.send(response);
    } else {
        
        const response = {
            attempts: updated.found ? request.attempts : request.attempts -1,
            word: request.word,
            guessed_letters: request.guessed_letters,
            wordToGuess: updated.array,
            completed: false
        }
        res.send(response);
    }
});

app.post('/guess-whole-word', (req, res) => {
    const request = req.body;
    if ((request.attempts === "undefined" || request.wholeWord === "undefined" || request.word === "undefined" || request.attempts === "undefined" || request.wordToGuess === "undefined"))
        throw new Error("find of undefined")


    if (!(request.word === request.wholeWord))
        res.send({ ...request, attempts: request.attempts - 2, completed: false })
    else {
        const done = request.word.split('').map(letter => letter)
        const response = {
            completed: true,
            wordToGuess: done,
        }
        res.send(response)
    }
});


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})


app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

// error handler middleware
app.use((error, req, res, next) => {
    res.status(error.status || 500).send({
        error: {
            status: error.status || 500,
            message: error.message || 'Internal Server Error',
        },
    });
});

module.exports = {
    app,
    isValidWord,
    isNewLetter,
    updateArray,
    isCompleted
};