const chai = require('chai'), chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
const should = chai.should();
const { app } = require('../src/index');
const serverGame = app;

describe('Test e2e', () => {
    it('Validate conect with server', (done) => {
        chai.request(serverGame)
            .get('/')
            .end((err, res) => {
                expect(res).to.have.status(200)
                res.text.should.be.eql('Welcome to my server');
                done();
            });
    })

    it("Set valid Word", (done) => {
        chai.request(serverGame)
            .post('/set-word')
            .send({
                word: "manzana"
            })
            .end((err, res) => {
                const response = {
                    valid: true,
                    attempts: 6,
                    word: "manzana",
                    guessed_letters: [],
                    wordToGuess: [
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-"
                    ],
                    completed: false
                }
                expect(res).to.have.status(200)
                expect(res).to.be.json

                chai.expect(res.body).to.eql({
                    ...response
                })
                done();
            });
    });

    it("Set invalid Word", (done) => {
        chai.request(serverGame)
            .post('/set-word')
            .send({
                word: "manzana pera"
            })
            .end((err, res) => {
                expect(res).to.have.status(400)
                expect(res).to.be.json

                chai.expect(res.body).to.eql({
                    error: 'Invalid word'
                })
                done();
            });
    });

    it("Guess letter", (done) => {
        chai.request(serverGame)
            .post('/guess-letter')
            .send({
                letter: "z",
                attempts: 6,
                word: "manzana",
                guessed_letters: [
                    "a",
                    "p",
                    "m",
                    "n"
                ],
                wordToGuess: [
                    "m",
                    "a",
                    "n",
                    "-",
                    "a",
                    "n",
                    "a"
                ]
            })
            .end((err, res) => {
                expect(res).to.have.status(200)
                expect(res).to.be.json

                chai.expect(res.body).to.eql({
                    attempts: 5,
                    word: "manzana",
                    guessed_letters: [
                        "a",
                        "p",
                        "m",
                        "n",
                        "z"
                    ],
                    wordToGuess: [
                        "m",
                        "a",
                        "n",
                        "z",
                        "a",
                        "n",
                        "a"
                    ],
                    completed: true
                })
                done();
            });
    });

    it("Invalid request Guess letter", (done) => {
        chai.request(serverGame)
            .post('/guess-letter')
            .send({
                letter: "z",
                guessed_letters: [
                    "a",
                    "p",
                    "m",
                    "n"
                ],
                wordToGuess: [
                    "m",
                    "a",
                    "n",
                    "-",
                    "a",
                    "n",
                    "a"
                ]
            })
            .end((err, res) => {
                expect(res).to.have.status(500)
                done();
            });
    });

    it("Guess whole word succes", (done) => {
        chai.request(serverGame)
            .post('/guess-whole-word')
            .send({
                wholeWord: "manzana",
                attempts: 6,
                word: "manzana",
                wordToGuess: [
                    "m",
                    "a",
                    "n",
                    "-",
                    "a",
                    "n",
                    "a"
                ]
            })
            .end((err, res) => {
                const response = {
                    completed: true,
                    wordToGuess: [
                        "m",
                        "a",
                        "n",
                        "z",
                        "a",
                        "n",
                        "a"
                    ]
                };
                expect(res).to.have.status(200)
                expect(res).to.be.json
                chai.expect(res.body).to.eql({...response})
                done();
            });
    });

    it("Guess whole word fail", (done) => {
        chai.request(serverGame)
            .post('/guess-whole-word')
            .send({
                wholeWord: "pera",
                attempts: 6,
                word: "manzana",
                wordToGuess: [
                    "m",
                    "a",
                    "n",
                    "-",
                    "a",
                    "n",
                    "a"
                ]
            })
            .end((err, res) => {
                const response = {
                    wholeWord: "pera",
                    attempts: 4,
                    word: "manzana",
                    wordToGuess: [
                        "m",
                        "a",
                        "n",
                        "-",
                        "a",
                        "n",
                        "a"
                    ],
                    completed: false
                }
                expect(res).to.have.status(200)
                expect(res).to.be.json
                chai.expect(res.body).to.eql(response)
                done();
            });
    });

});