const assert = require('assert');

const { isValidWord, isNewLetter, updateArray, isCompleted } = require('../src/index');



describe('Unit test', () => {
    /**
     * 
     * El método solo deve permitir ingresar una palabar
     * Si recibe más de una palabra debe retornar falso
     * 
     */
    describe('Test isValidWord method', () => {
        it('Returns true if it receives a word', () => {
            assert.strictEqual(isValidWord('manzana'), true);
        });
        it('Returns false if it receives more than one word', () => {
            assert.strictEqual(isValidWord('manzana pera'), false);
        });
        it('Return false if it reeives whitespace', () => {
            assert.strictEqual(isValidWord(''), false);
        });
    });

    /**
       * 
       * - El método solo deve validar que la letra que este ingresando
       * no se haya probado antes. Las letras que se hayan probado estarán en un array
       * 
       * - Si es una nueva letra retorna true, de lo contrario false
       * 
       */
    describe('Test isNewLetter method', () => {
        const letter = "a";
        const array_letters = []
        it('Returns true if is new letter', () => {
            assert.strictEqual(isNewLetter(array_letters, letter), true);
        });
        const array_letters2 = ['a']
        it('Returns false if letter already exist', () => {
            assert.strictEqual(isNewLetter(array_letters2, letter), false);
        });
    });

    /**
       * 
       * - Se actuliza el array que tiene la letras de la palabra a adivinar
       * 
       */
    describe('Test updateArray method', () => {
        const letter = "a";
        const word = 'manzana';
        const array_letters = ['m', '-', 'n', 'z', '-', 'n', '-']
        const expect_array = ['m', 'a', 'n', 'z', 'a', 'n', 'a']
        it('Return updated array', () => {
            assert.deepStrictEqual(updateArray(word, array_letters, letter), expect_array);
        });
        const expect_array_fail = ['m', 'a', 'n', 'z', 'a', 'n', '-']
        it('Return updated array', () => {
            assert.notDeepStrictEqual(updateArray(word, array_letters, letter), expect_array_fail);
        });

    });

    /**
       * 
       * - Valida si faltan letras por adivinar
       * 
       * -En el caso de no faltar ninguna letra retorna true, de lo contrario false
       * 
       */
    describe('Test isCompleted method', () => {
        const array_not_completed = ['m', '-', 'n', 'z', '-', 'n', '-']
        const array_completed = ['m', 'a', 'n', 'z', 'a', 'n', 'a']
        it('Returns true if no script is detected', () => {
            assert.strictEqual(isCompleted(array_completed), true);
        });
        it('Returns false if script is detected', () => {
            assert.strictEqual(isCompleted(array_not_completed), false);
        });
    });

});

