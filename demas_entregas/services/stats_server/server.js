const { GraphQLServer, MockList } = require('graphql-yoga');
const fetch = require('node-fetch');
const { v4: uuidv4 } = require('uuid');

//const fetchJson = (...args) => fetch(...args).then(response => response.json());

const typeDefs = `

type Word{
    word: String
}
type Player {
    name: String
}
input inputPlayer{
    name: String
}
input inputGame{
    id: String
    player: inputPlayer
    attempts: Int
    word: String
    guessed_letters: [String]
    wordToGuess: [String]
}


type Game {
    id: String
    player: Player
    attempts: Int
    word: String
    guessed_letters: [String]
    wordToGuess: [String]
}

type Query {
    GamesByName(playerName: String): [Game]
    Games: [Game]
    Words: [Word]
    AllPlayers: [Player]
    Player(name: String): Player
    Game(id: String): Game
}

type Mutation{
    setWord(word: String): String
    Game(game: inputGame): Game
    CreateGame(id: String, name:String): String
    addPlayer(name: String): Player
}
`;

const BASE_URL = 'https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/'
const _headers = {
    'Content-Type': 'application/json',
    'x-application-id': 'rigoberto.osorio'
};

const PREFIX_PLAYER = "player-";
const PREFIX_GAME = "game3";
const PREFIX_WORD = "word-";



const resolvers = {
    Query: {
        Game: async(_, { id })=> {
            const urlResource = `${BASE_URL}collections/${PREFIX_GAME}${id}`;
            const response = await fetch(urlResource, {
                method: "get",
                headers: _headers,
            });
            const res = await response.json();
            console.log('response ', res);
            const respuesta = res.map(item => JSON.parse(item.value))
            return respuesta[0]
        },
        GamesByName: async(root, { playerName }) => {
            const urlResource = `${BASE_URL}collections/${PREFIX_GAME}`;
            const response = await fetch(urlResource, {
                method: "get",
                headers: _headers,
            });
            const res = await response.json();
            const respuesta = res.map(item => JSON.parse(item.value))
            console.log('response',respuesta);
            return respuesta.filter(ele=>ele.player.name===playerName)
        },
        Games: async() => {
            const urlResource = `${BASE_URL}collections/${PREFIX_GAME}`;
            const response = await fetch(urlResource, {
                method: "get",
                headers: _headers,
            });
            const res = await response.json();
            console.log('reponse ', res);
            const respuesta = res.map(item => JSON.parse(item.value))
            return respuesta
        },
        Words: async () => {
            const urlResource = `${BASE_URL}collections/${PREFIX_WORD}`;
            const response = await fetch(urlResource, {
                method: "get",
                headers: _headers,
            });
            const res = await response.json();
            console.log('reponse ', res);
            return res.map(item => JSON.parse(item.value))
        },
        AllPlayers: async () => {
            const urlResource = `${BASE_URL}collections/${PREFIX_PLAYER}`;
            const response = await fetch(urlResource, {
                method: "get",
                headers: _headers,
            });
            const res = await response.json();
            console.log('reponse ', res);
            return res.map(item => JSON.parse(item.value))
        },
        Player: async (roor, { name }) => {
            const urlResource = `${BASE_URL}collections/${PREFIX_PLAYER}${name}`;
            const response = await fetch(urlResource, {
                method: "get",
                headers: _headers,
            });
            const res = await response.json();
            console.log('reponse ', res);
            return JSON.parse(res[0].value)
        }
    },
    Mutation: {
        setWord: async (root, { word }) => {
            const urlResource = `${BASE_URL}pairs/${PREFIX_WORD}${word}`;
            const response = await fetch(urlResource, {
                method: "put",
                headers: _headers,
                body: JSON.stringify({ word })
            });
            const res = await response.json();
            console.log('reponse ', res);
            const respuesta = JSON.parse(res.value)
            return respuesta.word;
        },
        Game: async(_, { game }) => {
            const urlResource = `${BASE_URL}pairs/${PREFIX_GAME}${game.id}`;
            const response = await fetch(urlResource, {
                method: "put",
                headers: _headers,
                body: JSON.stringify({ ...game })
            });
            const res = await response.json();
            console.log('reponse ', res);
            return JSON.parse(res.value);
        },
        CreateGame:async(_,{id,name}, context, info)=>{
            const urlResource = `${BASE_URL}pairs/${PREFIX_GAME}${id}`;
            const response = await fetch(urlResource, {
                method: "put",
                headers: _headers,
                body: JSON.stringify({ id, player:{name: name} })
            });
            const res = await response.json();
            console.log('id ', id);
            console.log('reponse ', res);
            const respuesta = JSON.parse(res.value)
            return respuesta.id;
        },
        addPlayer: async (_, { name }) => {
            const urlResource = `${BASE_URL}pairs/${PREFIX_PLAYER}${name}`;
            const response = await fetch(urlResource, {
                method: "put",
                headers: _headers,
                body: JSON.stringify({ name })
            });
            const res = await response.json();
            console.log('reponse ', res);
            return JSON.parse(res.value);
        }
    }
};

const server = new GraphQLServer({ typeDefs, resolvers })

server.start({
    playground: '/playground',
    port: 4000 // process.env.PORT
}, () => {
    console.log(
        `😄 Server running`
    );
})