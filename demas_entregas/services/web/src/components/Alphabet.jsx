import React from 'react'
const Alphabet = (props) => {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
    const buttons = alphabet.map(ele=>{
        const disabled  = props.intentos.includes(ele);
        return {value:ele, disabled} 
    })
    return (
        <>{
            buttons.map(ele=><button style={{width:'30px', height:'30px'}} key={ele.value} disabled={ele.disabled} onClick={()=>props.setLetter(ele.value)}>{ele.value}</button>)
        }
        </>
    );
}
 
export default Alphabet;