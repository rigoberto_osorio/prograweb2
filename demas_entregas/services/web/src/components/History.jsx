import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import Menu from '../components/Menu.jsx'
const History = () => {
    const [listGames,setListGames] = useState([])
    const user = localStorage.getItem('user');
    useEffect(()=>{
        const get_query = `
        query{
            GamesByName(playerName: "${user}"){
                attempts
                wordToGuess
                guessed_letters
                word
                player{
                    name
                }
                id
            }
        }
        `
        fetch(`http://localhost:4000`,{
            method:"post",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({query: get_query})
        })
        .then(res=>res.json())
        .then(response=>{
            setListGames(response.data.GamesByName) 
        })
    },[])
    const history = useHistory();
    const goGame=(game)=>{
      console.log(game);
      localStorage.removeItem('idGame')
      localStorage.setItem('idGame',game);
      history.push('/new')
    }
    return ( 
        <>
        <Menu/>
        <h2>Historial {user}</h2>
        <ul>
            {
                listGames.map(game=><li key={game.id} onClick={()=>goGame(game.id)}>{game.id}</li>)
            }
        </ul>
        </>
    );
}
 
export default History;