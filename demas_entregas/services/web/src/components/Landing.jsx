import React from "react";
import { Link } from "react-router-dom";
import styles from "./../app.css";
const Landing = () => {
  return (
    <div className={styles.main}>
      <h1>Entrega Final</h1>
      <p>
        Intenta iniciar sesión <Link to="/sign-in">aquí</Link>.
      </p>
    </div>
  );
};

export default Landing;
