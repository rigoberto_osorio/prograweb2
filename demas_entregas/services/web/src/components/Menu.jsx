import React, { useContext } from "react";
import { Link, Redirect, Route, Switch, useHistory, useRouteMatch } from "react-router-dom";
import Stats from "./Stats.jsx";

const Menu = () => {
  const { path, url } = useRouteMatch();

  if (!localStorage.getItem('user')) return <Redirect to="/sign-in" />;

  const exit = ()=>{
    history.push('/')
    localStorage.clear();
  }

  const history = useHistory();
  const newGame = ()=>{
    const config = {
      method: 'post',
      headers:{
        "Content-Type":"application/json"
      },
      body: JSON.stringify({user: localStorage.getItem('user')})
    }
    fetch(`http://localhost:3001/game`,config)
    .then((response)=>response.json())
    .then((resolve)=>{
      if(resolve.idGame){
        localStorage.setItem('idGame',resolve.idGame)
        console.log('Creado con valor', resolve);
        history.push('/new')
      }else{
        alert("Error al crear partida")
      }
      
    })
  }
  return (
    <div className="menu">
      <nav>
        <ul>
          <li>
          <Link to={`/game`}>Home</Link>
          </li>
          <li>
            <Link to={`/stats`}>Stats</Link>
          </li>
          <li>
            <span onClick={newGame}>New game</span>
          </li>
          <li>
            <Link to={`/history`}>Past games</Link>
          </li>
          <li>
            <span onClick={exit}>Exit</span>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Menu;
