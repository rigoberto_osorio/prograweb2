import React, {useEffect, useState} from "react";
import { useHistory } from "react-router";
import TextInput from "./TextInput.jsx";
import Alphabet from "./Alphabet.jsx";
import Word from "./word.jsx";
const Play = () => {
  const history = useHistory();
  if(!localStorage.getItem('user')){
    history.push('/');
  }

  useEffect(()=>{
    const config = {
      method: 'get',
      headers:{
        "Content-Type":"application/json"
      },
    }
    fetch(`http://localhost:3001/game/${localStorage.getItem('idGame')}`,config)
    .then((response)=>response.json())
    .then((resolve)=>{
      if(resolve.data.Game.word)
        setStateGame({...resolve.data.Game})
      console.log('Existe', resolve);
    })
  },[])
  const [formValues, setFormValues] = useState({ word: "" });
  const [error, setError] = useState(false);
  const [stateGame, setStateGame] = useState({ 
    valid: true,
        attempts: 6,
        word: '',
        guessed_letters: [],
        wordToGuess:[],
        completed:false
  });

  

  const handleInputChange = (event) => {
    const target = event.target;
    setFormValues({ ...formValues, [target.name]: target.value });
  };

  const goBack =()=>{
    localStorage.removeItem('idGame');
    history.goBack();
  }
  
  const handleSetLetter = (letter)=>{
    console.log(letter);
    const config = {
      method: 'post',
      headers:{
        "Content-Type":"application/json"
      },
      body: JSON.stringify({
        event: 'guessLetter',
        letter: letter,
        user: localStorage.getItem('user')
      })
    }
    fetch(`http://localhost:3001/game/${localStorage.getItem('idGame')}/event`,config)
    .then((response)=>response.json())
    .then((resolve)=>{
      setStateGame({...resolve})
      console.log('Creado con valor', resolve);
    })
  }

  const createGame = (e)=>{
    setError(false)
    e.preventDefault();
    const config = {
      method: 'post',
      headers:{
        "Content-Type":"application/json"
      },
      body: JSON.stringify({
        word: formValues.word,
        event: 'setWord',
        user: localStorage.getItem('user')
      })
    }
    fetch(`http://localhost:3001/game/${localStorage.getItem('idGame')}/event`,config)
    .then((response)=>response.json())
    .then((resolve)=>{
      console.log('Creado con valor', resolve);
      if(!resolve.error)
        setStateGame({...resolve})
        else
        setError(true)
    })
  }
  const x = <h1>hola</h1>
  return <>
  <div className="container">
    {
      stateGame.word=== ''&&
      <form onSubmit={createGame}>
    <TextInput
        label="Palabra"
        name="word"
        value={formValues.word}
        required={true}
        onChange={handleInputChange}
      />
      <button tupe="submit">Crear</button>
    </form>  
    }
    
    {error && 
      <h1>Solo puedes ingresar una palabra</h1>
    }
    {(stateGame.word !== '' && stateGame.attempts !==0 ) &&
      <>
      <h4>Intentos disponibles {stateGame.attempts}</h4>
      <br/>
      { !stateGame.completed &&
        <Alphabet intentos={stateGame.guessed_letters} setLetter={handleSetLetter}/>
      }
        
        <br/>
        <Word letters={stateGame.wordToGuess} />
        {
          stateGame.completed &&
          <h2>Ganaste</h2>
        }
      </>
    }
    {
      stateGame.attempts===0 &&
      <h1>Perdiste</h1>
    }
    <h1 onClick={goBack}>Volver</h1>
  </div>
  </>;
};

export default Play;
