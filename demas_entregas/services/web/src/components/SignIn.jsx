import React, { useState, useContext } from "react";
import { Redirect, useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import TextInput from "./TextInput.jsx";
import styles from './../app.css'
const SignIn = ({ returnTo }) => {
  const history = useHistory();
  const [formValues, setFormValues] = useState({ name: "" });


  const handleInputChange = (event) => {
    const target = event.target;
    setFormValues({ ...formValues, [target.name]: target.value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    localStorage.setItem('user',formValues.name)
    history.push('/game')
  };

  return (
    <div className={styles.login}>
    <form onSubmit={handleSubmit}>
      <TextInput
        label="Nombre"
        name="name"
        value={formValues.name}
        onChange={handleInputChange}
        required={true}
      />
      <br />
      <input className={styles.buttonSingIn} type="submit" />
    </form>
    </div>
  );
};
SignIn.propTypes = {
  returnTo: PropTypes.string.isRequired,
};

export default SignIn;
