import React from 'react'
const Word = (props) => {
    return (
        <>
        {
            props.letters.map((ele,index)=> <input key={ele+index} style={{width:'30px', height:'25px',  margin: '2rem 0.4rem'}} className="letter" type="text" value={ele} disabled={true}/>)
        }
        </>
     );
}
 
export default Word;