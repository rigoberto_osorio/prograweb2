import React, {useState} from "react";

const Form = (props) => {
    const [name, setName] = useState('');
    const [firstName, setFName] = useState('');
    const [password, setPassword] = useState('');
    const register = (e) => {
        e.preventDefault();
        props.parentCallBack(name, firstName, password);
    }
    return (
        <>
            <h1>Registro</h1>
            <form onSubmit={register}>
                <div>
                    <label style={{fontSize: "18px"}}>
                        Nombre cuenta:<br/>
                    <input required type="text" value={name} onChange={(e)=>setName(e.target.value)} name="name" />
                    </label>
                </div>
                <div>
                    <label style={{fontSize: "18px"}}>
                        Nombre:<br/>
                    <input required type="text" onChange={(e)=>setFName(e.target.value)} name="firstName" />
                    </label>
                </div>
                <div>
                    <label style={{fontSize: "18px"}}>
                        Contraseña:<br/>
                        <input required type="password" onChange={(e)=>setPassword(e.target.value)} name="password" />
                    </label>
                </div>
                <input style={{padding: "10px", marginTop: "25px"}} type="submit" value="Aceptar" />
            </form>
        </>);
}

export default Form;