import React, {Component} from "react";
import LoginComponent from '../Components/Login/FormLogin'

class Login extends Component {

    logUser(name, password) {
        const data = {
            password
        }
        fetch(`https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/${name}/sessions`,{
            method: 'POST',
            body: JSON.stringify(data),
        }).then((response) => response.json()).then((user)=>console.log(user))
        
    }
    render() {
        return ( 
        <div>
            <LoginComponent parentCallBack={this.logUser} />
        </div> );
    }
}
 
export default Login;