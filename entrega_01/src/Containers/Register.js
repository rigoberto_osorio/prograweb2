import React, {Component} from "react";
import RegisterComponent from '../Components/Register/Form'

class Register extends Component {
    
    createUser(name, firstName, password) {
        const data = {
            firstName,
            password
        }
        fetch(`https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/${name}`,{
            method: 'POST',
            body: JSON.stringify(data),
        }).then((response) => response.json()).then((user)=>console.log(user))
        
        
    }
    render() {
        return ( 
        <div>
            <RegisterComponent parentCallBack={this.createUser} />
        </div> );
    }
}
 
export default Register;