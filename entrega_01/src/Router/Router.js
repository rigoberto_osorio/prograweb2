import React from 'react';
import {
    BrowserRouter,
    Switch,
    Route
  } from 'react-router-dom';
import Register from '../Containers/Register'
import Login from '../Containers/Login'

const Router = () => {
    return (
       <BrowserRouter>
       <Switch>
            <Route exact path="/">
                <Login/>
            </Route>
            <Route exact path="/register">
                <Register />
            </Route>
        </Switch>
       </BrowserRouter>
    )
}

export default Router;
