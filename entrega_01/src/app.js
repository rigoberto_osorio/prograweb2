import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Register from './Containers/Register'
import Login from './Containers/Login'
function App() {
  return (
    <Router>
      <Route exact path="/"><h1>hola</h1></Route>
      <Route path="/register" component={Register} />
      <Route path="/login" component={Login} />

    </Router>
  )
}

export default App;