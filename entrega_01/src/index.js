import React from 'react';
import _ from 'lodash';
import ReactDom from 'react-dom';
import App from './app';

ReactDom.render(
        <App /> 
      ,
    document.getElementById('root')
  );
  